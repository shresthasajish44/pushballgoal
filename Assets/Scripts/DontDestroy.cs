using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class DontDestroy : MonoBehaviour
{
    public TMP_Text goalScore;

    public int currentScore = 0;
    // Start is called before the first frame update
    void Start()
    {
        goalScore.text = currentScore.ToString();
    }

    private void Awake()
    {
        int DontDestroyCount = FindObjectsOfType<DontDestroy>().Length;

        if (DontDestroyCount > 1)
        {
            Destroy(gameObject);
        }
        else {
            DontDestroyOnLoad(gameObject);
        }
    }

    public void AddScore() {
        currentScore = currentScore + 1;
        goalScore.text = currentScore.ToString();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
