using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerManager : MonoBehaviour
{
    public InputAction playerController;
    public Vector2 moveDirection;
    public float playerSpeed;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

   

    private void OnEnable()
    {
        playerController.Enable();
    }

    private void OnDisable()
    {
        playerController.Disable();
    }

    // Update is called once per frame
    void Update()
    {
        moveDirection = playerController.ReadValue<Vector2>();
        float x = transform.position.x;
        float z = transform.position.z;

        if (playerController.IsPressed()) {
            transform.Translate(new Vector3(moveDirection.x , 0f, moveDirection.y ) * playerSpeed * Time.deltaTime);
        }
    }
}
